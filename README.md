# Przepiękna symulacja na niezawodności inżynieryję
by Jan Długosz & M.D & M.D.
## Docs:

### postprocess.ipynb
postprocess jupyter notebook

### elegancki.py
sim engine
#### Run(breakdownIntensities, renewIntensities, endTime, timeStep, ID)
runs one iteration of system as shown in _kzn.png_
##### breakdownIntensities
- an araray of breakdown intensities like this:
```
[
[lambdaM1, lambdaA1],
[lambdM2, lambdaA2],
[lambdaM3, lambdaA3]
]
```
- an araray of renew intensities like this:
```
[
[renewM1, renewA1],
[lambdM2, renewA2],
[renewM3, renewA3]
]
```
##### endTime
- time horizon for iteration

##### timeStep
- time step for iteration

##### ID
- iteration ID

#### sim(l1, l2, endTime, timeStep, iterations)
runs many iterations of run() and returns the vector of system age at sim breaking

##### l1
- breakdown intensity that defines breakdownIntensities for all elements

##### l2
- renew intensity that defines renewIntensities for all elements

##### endTime
- same as in run()

##### timeStep
- same as in run()

##### iterations
- numer of times run() will be called
