import numpy as np
from pprint import pprint

class system:
    age = 0
    state = True
    dt = None

    def __init__(self, breakdownIntensity, renewIntensities, dt):
        self.dt = dt
        self.a1 = assemblySerial(self, breakdownIntensity[0], renewIntensities[0], dt)
        self.a2 = assemblySerial(self, breakdownIntensity[1], renewIntensities[1], dt)
        self.a3 = assemblySerial(self, breakdownIntensity[2], renewIntensities[2], dt)

    def getState(self):
        if self.a1.getState()+self.a2.getState()+self.a3.getState() >= 2: #ugh
            return True
        else:
            return False

    def checkState(self):
        self.a1.checkState()
        self.a2.checkState()
        self.a3.checkState()
        if self.a1.getState()+self.a2.getState()+self.a3.getState() >= 2: #ugh
            self.state = True
        else:
            self.state = False
    def newRND(self):
        self.a1.newRND()
        self.a2.newRND()
        self.a3.newRND()
    def incAge(self):
        self.a1.incAge()
        self.a2.incAge()
        self.a3.incAge()
        self.age = self.age + self.dt
class assemblySerial:
    age = 0
    state = True
    dt = None

    def __init__(self, parent, breakdownIntensity, renewIntensity, dt):
        self.e1 = element(self, breakdownIntensity[0], renewIntensity[0], dt)
        self.e2 = element(self, breakdownIntensity[1], renewIntensity[1], dt)
        self.dt = dt
        self.parent = parent
    def getState(self):
        if self.e1.getState() and self.e2.getState():
            return True
        else:
            return False
    def checkState(self):
        self.e1.checkState()
        self.e2.checkState()
        if self.e1.getState() and self.e2.getState():
            if self.state == False:
                self.age = 1
            self.state = True
        else:
            if self.state == True:
                self.age = 1
            self.state = False

    def incAge(self):
        self.age = self.age + self.dt
        self.e1.incAge()
        self.e2.incAge()
    def newRND(self):
        self.e1.newRND()
        self.e2.newRND()
class element:
    dt = 0
    state = True
    age = 0
    breakdownP = 0
    renewP = 0
    RND = 0
    def __init__(self, parent, breakdownIntensity, renewIntensity, dt):
        self.breakdownP = 1-np.e**(-breakdownIntensity*dt)
        self.renewP = 1-np.e**(-renewIntensity*dt)
        self.parent = parent
        self.dt = dt
    def checkState(self):
        if self.state == True:
            if self.RND < self.breakdownP:
                self.breakdown()
        elif self.state == False:
            if self.RND < self.renewP:
                self.repair()
    def newRND(self):
        self.RND = np.random.rand()
    def breakdown(self):
        self.state = False
        self.age = 1
    def repair(self):
        self.state = True
        self.age = 1
    def getState(self):
        return self.state
    def incAge(self):
        self.age = self.age + self.dt
def makeMatrix(var):
    return [[var, var],[var, var],[var, var]]
def makeMatrix(var, var2):
    return [[var, var2], [var, var2], [var, var2]]

def run(breakdownIntensities, renewIntensities, endTime, timeStep, ID):
    sys = system(breakdownIntensities, renewIntensities, timeStep)
    for i in range(endTime):
        states = [sys.a1.e1.state*1, 1*sys.a1.e2.state,
                  sys.a2.e1.state*1, 1*sys.a2.e2.state,
                  sys.a3.e1.state*1, 1*sys.a3.e2.state]
        if sys.getState() == False:
            print('{}, so breaking iteration {} @{}'.format(states, ID, i))
            return i
            break
        sys.newRND()
        sys.checkState()
    #    print(states)
    else:
        print('sim {} ran to end time'.format(ID))
        print(states)
        return endTime

def sim(l, l2, endTime, timeStep, iterations):
#    brkInt = makeMatrix(l)
#    renInt = makeMatrix(l2)
    wyniki = np.zeros([iterations, 2])
    for iteration in range(iterations):
        wyniki[iteration, 1]=run(l, l2, endTime, timeStep, iteration)
        wyniki[iteration, 0]=iteration
    return wyniki
